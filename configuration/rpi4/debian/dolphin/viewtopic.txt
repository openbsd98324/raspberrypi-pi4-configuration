   Link: canonical
   [ ]
     * For home
     * For industry
     * Hardware
     * Software
     * Documentation
     * News
     * Forums
     * Foundation

  xterm 

  xterm -bg black 


     * Forum FAQ
     * About
          * About us
          * Contact us
          * Documentation
          * Trademark rules
   _____________________ [ Submit ]
     * Login
     * Register
     *    * * Unanswered topics
          * Active topics
          * * Forum FAQ
          * About
               * About us
               * Contact us
               * Documentation
               * Trademark rules
     * Board index  Projects  Gaming

Building Dolphin (GC/WII Emulator) on the Pi 4: 2021 Edition

   Post Reply
     * Print view
   _____________________ [ Submit ]
   21 posts • Page 1 of 1

   User avatar
   bomblord
           Posts: 327
           Joined: Sun Jul 14, 2019 2:54 am

  Building Dolphin (GC/WII Emulator) on the Pi 4: 2021 Edition

     * Quote

   Fri Oct 29, 2021 7:47 pm

   Installing Dolphin on your Pi 4: Foreword

   Good day to everyone how are you? Good? Hope so....

   Anyway, there are some out of date instructions out there on building Dolphin on the Pi and since Vulkan 1.1 is available (yay!) it's a good time to update everything and bring in some
   new discussion.

   Once again DO NOT SUDO APT INSTALL DOLPHIN-EMU it is even further out of date than the last time and even more thousands of commits behind.

   Also you will need to use a 64 bit OS I used raspberry pi os https://downloads.raspberrypi.org/raspios_arm64/images/

   Also also you will want to build Vulkan if you intend to use it because for some reason it's not available in the 64 bit os by default https://gitlab.freedesktop.org/mesa/mesa/.... I'm
   not going to get into this because it's probably more complicated than this post there are other posts on this forum that go over it although they might be out of date and I'm not 100%
   sure how I got it to build and work a lot of dependencies and grabbing bits and pieces from other online discussions.

   Installation

   Code: Select all

 sudo apt update

 sudo apt upgrade

 sudo apt install --no-install-recommends ca-certificates qtbase5-dev qtbase5-private-dev git cmake make gcc g++ pkg-config libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libxi-dev libxrandr-dev libudev-dev libevdev-dev libsfml-dev libminiupnpc-dev libmbedtls-dev libcurl4-openssl-dev libhidapi-dev libsystemd-dev libbluetooth-dev libasound2-dev libpulse-dev libpugixml-dev libbz2-dev libzstd-dev liblzo2-dev libpng-dev libusb-1.0-0-dev gettext

   I haven't compared this vs the old instructions but I've copied/pasted the dependencies that dolphin lists on their website.This next part definitely has a new instruction due to the
   mgba add.

   Code: Select all

 git clone https://github.com/dolphin-emu/dolphin.git

 cd dolphin-emu

 //next instruction is new
 git submodule update --init

 mkdir build && cd build

 sudo cmake ..

 sudo make

   Wait

   At this point you're gonna be waiting. Go make some coffee and start on the pi zero 2 project you swore you would get done one day.
   ....
   ....
   Back? Is it done yet? No?... yeah this takes a while go get more coffee.
   ....
   ....

   Finish waiting
   Hopefully it completed without errors and you're not too jittery from the coffee. Now, 2 more commands to finish up

   Code: Select all

 sudo make install

 sudo reboot

   Configuration

   Now onto configuration... which isn't much as we now have Vulkan 1.1. This time no file edits need to be made or special flags set. Just open dolphin either from the games menu or from
   terminal and go to the graphics tab and where it says OpenGL click the drop down and choose Vulkan. If you opted not to use Vulkan the old instructions to prefer GLES need to be
   applied.

   Overclock

   This is technically optional from a "does it open without crashing" perspective but not from a "Will it run higher than 15fps" perspective. But maybe cinematic slow down makes the game
   feel more epic to you. Maybe you want to really dig into that N64 nostalgia. If that's you just ignore this next part.

   Code: Select all

 sudo nano /boot/config.txt

 //at the very bottom put

 over_voltage = 5
 arm_freq = 2000
 gpu_freq = 600

 //Ctrl+X
 //Enter

 sudo reboot

   Both the CPU/GPU can go higher but these are nice and stable clocks on every pi I've tested.

   Finished




